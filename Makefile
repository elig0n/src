# Makefile for src
#
# SPDX-FileCopyrightText: Copyright Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

prefix?=/usr/local
mandir?=/share/man
target=$(DESTDIR)$(prefix)

VERS=$(shell ./src version | sed -n -e "/src: /s///p")

SOURCES = README.adoc INSTALL.adoc COPYING NEWS.adoc TODO.adoc src srctest tapview src.adoc FAQ.adoc Makefile control

all: src.1

check: pylint shellcheck
	@./srctest | ./tapview

coverage:
	@./srctest -c coverage-data | ./tapview
	@coverage report --data-file=coverage-data | grep src
	@coverage html --data-file=coverage-data

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

FAQ.html: FAQ.adoc
	asciidoctor -a toc FAQ.adoc

clean:
	rm -f  *~ *.1 *.html *.tar.gz MANIFEST coverage-data
	rm -fr .rs* typescript test/typescript htmlconv

src-$(VERS).tar.gz: $(SOURCES) src.1
	@ls $(SOURCES) src.1 | sed s:^:src-$(VERS)/: >MANIFEST
	@(cd ..; ln -s src src-$(VERS))
	(cd ..; tar -czf src/src-$(VERS).tar.gz `cat src/MANIFEST`)
	@(cd ..; rm src-$(VERS))

pylint:
	@pylint --score=n src

shellcheck:
	@-shellcheck -s sh -f gcc srctest

fixme:
	@if command -v rg >/dev/null; then \
		rg --no-heading FIX''ME; \
	else \
		find . -type f -exec grep -n FIX''ME {} /dev/null \; | grep -v "[.]git"; \
	fi

dist: src-$(VERS).tar.gz

NEWSVERSION=$(shell sed -n <NEWS.adoc '/^[0-9]/s/:.*//p' | head -1)

version:
	@echo "Internal version:" $(VERS) "NEWS file version:" $(NEWSVERSION)

release: src-$(VERS).tar.gz src.html FAQ.html
	@[ $(VERS) = $(NEWSVERSION) ] || { echo "Version mismatch!"; exit 1; }
	shipper version=$(VERS) | sh -e -x

refresh: src.html FAQ.html
	@[ $(VERS) = $(NEWSVERSION) ] || { echo "Version mismatch!"; exit 1; }
	shipper -N -w version=$(VERS) | sh -e -x

install: all
	install -d "$(target)/bin"
	install -d "$(target)$(mandir)/man1"
	install src "$(target)/bin"
	install -m644 src.1 "$(target)$(mandir)/man1"

uninstall:
	rm "$(target)/bin/src" "$(target)$(mandir)/man1/src.1"
